**Cash Accounts** is a naming system that can be used alongside regular bitcoin addresses and payment codes to simplify the process of sharing payment information.

*Read the [Cash Account Specification](SPECIFICATION.md)*